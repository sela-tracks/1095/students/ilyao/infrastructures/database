# TokyoDB MongoDB Helm Chart Installation Guide

This guide will walk you through the process of installing and uninstalling the TokyoDB MongoDB Helm chart. Before starting, please ensure you have Helm installed and configured.

## Prerequisites

- Helm: Install Helm by following the instructions at [Helm Installation Guide](https://helm.sh/docs/intro/install/).

## Installation

Clone this repository to your local machine:

   ```bash
   git clone https://gitlab.com/sela-tracks/1095/students/ilyao/infrastructures/database.git
   ```
Navigate to the root directory of the cloned repository:
```
cd database
```
Open the values.yaml file in a text editor:
```
nano values.yaml
```
Or if you prefer vi text editor:
```
vi values.yaml
```
In the values.yaml file, locate the following section and replace the placeholder values with your desired credentials:
```
auth:
  enabled: true
  rootUser: root
  rootPassword: "CHANGEME"

  username: "username"
  password: "CHANGEME"
  database: "tokyodb"
```
Replace "CHANGEME" with your preferred passwords for the root user and the specified username.
#
#
Install the TokyoDB MongoDB Helm chart using Helm:
```
helm install tokyo-db oci://registry-1.docker.io/bitnamicharts/mongodb -n default -f ./values.yaml
```
Wait for the MongoDB deployment to complete. You can check the status using kubectl get pods.

Export the MongoDB root password to a variable:

    export MONGODB_ROOT_PASSWORD=$(kubectl get secret tokyo-db-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 -d)

## Uninstallation

To uninstall the TokyoDB MongoDB Helm chart, follow these steps:
Uninstall the Helm release:

    helm uninstall tokyo-db -n default
Wait for the MongoDB resources to be deleted. You can check the status using kubectl get pods.

The TokyoDB MongoDB Helm chart and its resources are now removed from your cluster.
